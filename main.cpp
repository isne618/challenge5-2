#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "tree.h"
using namespace std;

void main()
{
	Tree<string> mytree;
	ifstream file("text.txt");
	string sentence, word;
	vector<string>content;

	//output from text file
	int line = 1; //line start with 1
	while (getline(file, sentence)) 
	{
		cout << sentence << endl; //output text from file by line
		content.push_back(sentence); //pushback text to vector
		stringstream word_line(sentence);
		while (word_line >> word)
		{
			word = word + " " + to_string(line) + "\n"; //separate word and word's line
			mytree.insert(word);//insert word to binary tree
		}
		line++;
	}
	cout << "----------------------" << endl;
	mytree.inorder();

	/*for (int i = 0; i < content.size(); i++)
	{
		mytree.insert(content.at(i)); //insert text to binary tree
	}
	
	mytree.inorder();*/
	
}